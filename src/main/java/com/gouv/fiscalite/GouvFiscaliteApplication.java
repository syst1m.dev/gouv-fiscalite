package com.gouv.fiscalite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GouvFiscaliteApplication {

	public static void main(String[] args) {
		SpringApplication.run(GouvFiscaliteApplication.class, args);
	}

}
