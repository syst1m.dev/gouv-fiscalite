package com.gouv.fiscalite.gestion.societe;

import com.gouv.fiscalite.admin.societe.SocieteCategories;

public class TauxImpot {

    private final SocieteCategories societeCategorie;
    private final double taux;

    /**
     * Constructeur
     */
    public TauxImpot(SocieteCategories societeCategorie){

        this.societeCategorie = societeCategorie;
        this.taux = this.societeCategorieTaux(societeCategorie);

    }

    /**
     * getter taux
     */
    public double getTaux(){
        return this.taux;
    }

    /**
     * Taux en fonction de la catégorie de la société
     */
   public double societeCategorieTaux(SocieteCategories societeCategorie){
        double taux = 0;
        switch(societeCategorie) {
            case AUTO_ENTREPRISE:
                taux = 0.25d;
                break;
            case SAS:
                taux = 0.33d;
                break;
        }
        return taux;
    }



}
