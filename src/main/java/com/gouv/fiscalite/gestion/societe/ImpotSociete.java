package com.gouv.fiscalite.gestion.societe;

import org.springframework.lang.NonNull;

import com.gouv.fiscalite.admin.societe.Societe;
import com.gouv.fiscalite.exceptions.IncorrectChiffreDaffaireException;


public class ImpotSociete {

    private final int annee;
    private final Double chiffreDaffaire;
    private final double impot;

    /**
     * Constructeur
     */
    public ImpotSociete(int annee, Double chiffreDaffaire, Societe societe) throws IncorrectChiffreDaffaireException{
        this.annee = annee;
        this.chiffreDaffaire = chiffreDaffaire;
        this.impot = this.evalImpotSociete(chiffreDaffaire, societe);

    }

    /**
     * getter annee
     */
    public int getAnnee(){
        return this.annee;
    }

    /**
     * getter impot
     */
    public double getImpot(){
        return this.impot;
    }

    /**
     * Evalution de l'impot sur la société en fonction du Chiffre d'affaire et l'objet Societe
     */
    public double evalImpotSociete(@NonNull Double chiffreDaffaire, Societe societe) throws IncorrectChiffreDaffaireException {

        double evalImpot = 0;

        TauxImpot tauxImpot = new TauxImpot(societe.getCategorie());
        double taux = tauxImpot.getTaux();

        if(chiffreDaffaire > 0) {
            evalImpot = taux * chiffreDaffaire;
            return evalImpot;
        } else {
            throw new IncorrectChiffreDaffaireException("Chiffre d'affaire Incorrect : " + chiffreDaffaire);
        }


    }



}
