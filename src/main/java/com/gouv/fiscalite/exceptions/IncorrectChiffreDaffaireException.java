package com.gouv.fiscalite.exceptions;

import java.lang.Exception;

public class IncorrectChiffreDaffaireException extends Exception {
    public IncorrectChiffreDaffaireException(String errorMessage) {
        super(errorMessage);
    }
}
