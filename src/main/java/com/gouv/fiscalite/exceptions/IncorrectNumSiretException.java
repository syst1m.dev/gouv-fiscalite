package com.gouv.fiscalite.exceptions;

import java.lang.Exception;
//import java.lang.Throwable;

public class IncorrectNumSiretException extends Exception{

    public IncorrectNumSiretException(String errorMessage) {
        super(errorMessage);
    }

}
