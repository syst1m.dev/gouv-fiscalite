package com.gouv.fiscalite.admin.societe;

public class AutoEntreprise extends Societe {
    public AutoEntreprise(String siret, String denomination) {
        super(siret, denomination, SocieteCategories.AUTO_ENTREPRISE);
    }
}
