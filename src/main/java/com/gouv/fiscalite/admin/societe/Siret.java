package com.gouv.fiscalite.admin.societe;

import com.gouv.fiscalite.exceptions.IncorrectNumSiretException;
import org.springframework.lang.NonNull;

public class Siret {

    private final String numSiret;

    /**
     * Constructeur
     */
    public Siret(String numSiret) {

        this.numSiret = numSiret;

    }


    /**
     * getter numSiret
     */
    public String getNumSiret() {
        return this.numSiret;
    }

    /**
     * Verification de l'authenticité du SIRET
     */
    public boolean verifNumSiret(@NonNull String numSiret) throws IncorrectNumSiretException {

        boolean isNumSiretValid = false;
        if (numSiret.isEmpty() || numSiret.length() != 14) {
            isNumSiretValid = false;
            throw new IncorrectNumSiretException("Siret Incorrect : " + numSiret);
        } else {
            isNumSiretValid = true;
            return isNumSiretValid;
        }

    }

}
