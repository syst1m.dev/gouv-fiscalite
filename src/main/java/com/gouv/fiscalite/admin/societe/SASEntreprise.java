package com.gouv.fiscalite.admin.societe;

public class SASEntreprise extends Societe {

    private final SiegeSocial siegeSocial;

    public SASEntreprise (String siret, String denomination, SiegeSocial siegeSocial) {
        super(siret, denomination, SocieteCategories.SAS);
        this.siegeSocial = siegeSocial;
    }

    public SiegeSocial getSiegeSocial(){
        return this.siegeSocial;
    }


}
