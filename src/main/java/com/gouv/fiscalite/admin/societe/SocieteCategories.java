package com.gouv.fiscalite.admin.societe;

public enum SocieteCategories {
    AUTO_ENTREPRISE,
    SAS,
    SASU,
    SARL
}
