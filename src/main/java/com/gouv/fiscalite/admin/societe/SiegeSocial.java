package com.gouv.fiscalite.admin.societe;

public class SiegeSocial {


    private String nom;
    private String adresse;

    /**
     * Constructeur
     */
    public SiegeSocial(String adresse) {
        this.nom ="";
        this.adresse = adresse;
    }
    public SiegeSocial(String nom, String adresse) {
        this.nom = nom;
        this.adresse = adresse;
    }

    /**
     * getter nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * setter nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * getter adresse
     */
    public String getAdresse() {
        return this.adresse;
    }

    /**
     * Setter adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }


}
