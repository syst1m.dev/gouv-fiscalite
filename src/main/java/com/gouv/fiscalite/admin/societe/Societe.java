package com.gouv.fiscalite.admin.societe;

import com.gouv.fiscalite.admin.societe.Siret;

public class Societe {

    private final Siret siret;
    private final String denomination;
    private final SocieteCategories societeCategorie;

    /**
     * Constructeur
     */
    public Societe(String siret, String denomination, SocieteCategories societeCategorie) {
        this.siret = new Siret(siret);
        this.denomination = denomination;
        this.societeCategorie = societeCategorie;
    }

    /**
     * getter siret
     */
    public Siret getSiret(){
        return this.siret;
    }

    /**
     * getter denomination
     */
    public String getDenomination(){
        return this.denomination;
    }

    /**
     * getter Catégorie de la Société
     */
    public SocieteCategories getCategorie(){
        return this.societeCategorie;
    }

}
