package com.gouv.fiscalite.gestion.societe;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.gouv.fiscalite.admin.societe.SiegeSocial;
import com.gouv.fiscalite.gestion.societe.ImpotSociete;
//import com.gouv.fiscalite.admin.societe.SASEntreprise;
import com.gouv.fiscalite.admin.societe.SASEntreprise;

import com.gouv.fiscalite.exceptions.IncorrectChiffreDaffaireException;
import java.lang.NullPointerException;

public class ImpotSocieteTests {

     /*
     *
     * fonction => evalImpotSociete
     * test d'un simple retour 0
     *
     */
    /*@Test
    public void evalImpotSocieteReturn0() {

        ImpotSociete impotSociete = new ImpotSociete();
        double evalIS = impotSociete.evalImpotSociete();

        assertEquals(0 , evalIS);


    }*/

    /*
     *
     * fonction => evalImpotSociete
     * test return taux
     *
     */
    /*@Test
    public void evalImpotSocieteReturnTaux() {

        String numSiret = new String();
        String denomination = new String();
        SiegeSocial siegeSocial = new SiegeSocial("2 avenue Jean Lebas 93140");

        numSiret = "84354727400012";
        denomination = "SMART'EAZ";


        SASEntreprise societe = new SASEntreprise(numSiret, denomination, siegeSocial);
        ImpotSociete impotSociete = new ImpotSociete();
        double evalIS = impotSociete.evalImpotSociete(33000 , societe);

        assertEquals(0.33d , evalIS);


    }*/

    /*
     *
     * fonction => evalImpotSociete
     * test return evalImpot
     *
     */
    @Test
    public void evalImpotSocieteReturnEvalImpot() throws IncorrectChiffreDaffaireException {

            String numSiret = new String();
            String denomination = new String();
            SiegeSocial siegeSocial = new SiegeSocial("22 AV DE WAGRAM 75008 PARIS");

            numSiret = "55208131766522";
            denomination = "ELECTRICITE DE FRANCE (E.D.F.)";

            SASEntreprise societe = new SASEntreprise(numSiret, denomination, siegeSocial);

                ImpotSociete impotSociete = new ImpotSociete(2021 , 33000d , societe);
                //System.out.println(impotSociete.getAnnee());
                assertEquals(10890d , impotSociete.getImpot());


    }

    /*
     *
     * fonction => evalImpotSociete
     * le CA NonNull
     *
     */
    @Test
    public void evalImpotSocieteChiffreDaffaireNull() {
        assertThrows(NullPointerException.class, () -> {

            String numSiret = new String();
            String denomination = new String();
            SiegeSocial siegeSocial = new SiegeSocial("22 AV DE WAGRAM 75008 PARIS");

            numSiret = "55208131766522";
            denomination = "ELECTRICITE DE FRANCE (E.D.F.)";

            SASEntreprise societe = new SASEntreprise(numSiret, denomination, siegeSocial);
            ImpotSociete impotSociete = new ImpotSociete(2021 , null , societe);

        });

    }

    /*
     *
     * fonction => evalImpotSociete
     * le CA > 0
     *
     */
    @Test
    public void evalImpotSocieteChiffreDaffairePositif() {
        assertThrows(IncorrectChiffreDaffaireException.class, () -> {

            String numSiret = new String();
            String denomination = new String();
            SiegeSocial siegeSocial = new SiegeSocial("22 AV DE WAGRAM 75008 PARIS");

            numSiret = "55208131766522";
            denomination = "ELECTRICITE DE FRANCE (E.D.F.)";

            SASEntreprise societe = new SASEntreprise(numSiret, denomination, siegeSocial);
            ImpotSociete impotSociete = new ImpotSociete(2021 , -33000d , societe);

        });
    }

     /*
      * Test
      * Constructor => ImpotSociete
      *
      */
    @Test
    public void ImpotSocieteConstructor() throws IncorrectChiffreDaffaireException {

            String numSiret = new String();
            String denomination = new String();
            SiegeSocial siegeSocial = new SiegeSocial("22 AV DE WAGRAM 75008 PARIS");

            numSiret = "55208131766522";
            denomination = "ELECTRICITE DE FRANCE (E.D.F.)";


            SASEntreprise societe = new SASEntreprise(numSiret, denomination, siegeSocial);
            ImpotSociete impotSociete = new ImpotSociete(2021 , 33000d , societe);
            //System.out.println(impotSociete.getAnnee());
            assertEquals(10890d , impotSociete.getImpot());


    }

}
