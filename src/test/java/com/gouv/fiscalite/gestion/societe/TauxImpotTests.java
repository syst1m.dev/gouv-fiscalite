package com.gouv.fiscalite.gestion.societe;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.gouv.fiscalite.gestion.societe.TauxImpot;
import com.gouv.fiscalite.admin.societe.SocieteCategories;

public class TauxImpotTests {

     /*
     *
     * fonction => societeCategorieTaux
     * test d'un simple retour 0
     *
     */
   /*@Test
    public void tauxReturn0() {


        TauxImpot tauxImpot = new TauxImpot(SocieteCategories.AUTO_ENTREPRISE);
        double taux = tauxImpot.getTaux();
        assertEquals(0 , taux);


    }*/

    /*
     *
     * fonction => societeCategorieTaux
     * test switch societeCategorie
     *
     */
    @Test
    public void tauxSocieteCategorieSwitch() {


        TauxImpot tauxImpot = new TauxImpot(SocieteCategories.SAS);
        double taux = tauxImpot.getTaux();
        assertEquals(0.33d , taux);


    }


}
