package com.gouv.fiscalite.admin.societe;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.gouv.fiscalite.exceptions.IncorrectNumSiretException;
import java.lang.NullPointerException;

import com.gouv.fiscalite.admin.societe.Siret;

public class SiretTests {

    /*
     *
     * fonction => verifNumSiret
     * test d'un simple retour de boolean "false"
     *
     */
    @Test
    public void verifNumSiretSimpleFalseReturn(){
        assertThrows(IncorrectNumSiretException.class, () -> {
            Siret siret = new Siret("auah34");
            boolean verifNumSiret = siret.verifNumSiret(siret.getNumSiret());
            //assertEquals(false , verifNumSiret);
        });
    }

    /*
     *
     * fonction => verifNumSiret
     * test d'un numéro de Siret Vide
     *
     */
    @Test
    public void verifNumSiretIsEmpty(){
        assertThrows(IncorrectNumSiretException.class, () -> {
            Siret siret = new Siret("");
            boolean verifNumSiret = siret.verifNumSiret(siret.getNumSiret());
            //assertEquals(false , verifNumSiret);
        });
    }

    /*
     *
     * fonction => verifNumSiret
     * test sur la taille d'un numéro de Siret
     * Le SIRET se compose de 14 chiffres.
     *
     */
    @Test
    public void verifNumSiretLength(){
        assertThrows(IncorrectNumSiretException.class, () -> {
            Siret siret = new Siret("87770172200");
            boolean verifNumSiret = siret.verifNumSiret(siret.getNumSiret());
            //assertEquals(false , verifNumSiret);
        });
    }

    /*
     *
     * fonction => verifNumSiret
     * test SIRET non null
     *
     */
    @Test
    public void verifNumSiretNonNull(){
        assertThrows(NullPointerException.class, () -> {
            Siret siret = new Siret(null);
            boolean verifNumSiret = siret.verifNumSiret(siret.getNumSiret());
            //assertEquals(false , verifNumSiret);
        });
    }



}
