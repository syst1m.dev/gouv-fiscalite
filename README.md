----------- ENONCÉ ----------- 
- Le ministère des finances vous demande de créer un programme devant permettre de calculer les impôts dus par les entreprises françaises.

Dans un premier temps, ce programme devra gérer 2 types d'entreprise :

A) Les auto-entreprises, qui ont les propriétés suivantes :
- N° SIRET
- Dénomination

B) Les SAS, qui ont les propriétés suivantes :
- N° SIRET
- Dénomination
- Adresse du siège social

Le programme sera étendu par la suite avec d'autres types d'entreprise (SASU, SARL ...)

Par ailleurs, le calcul des impôts devra respecter les règles de gestion suivantes :
- Pour les auto-entreprises :
--> impôts = 25% du CA annuel de l'entreprise
- Pour les SAS :
--> impôts = 33% du CA annuel de l'entreprise

----------- ###### ----------- 

programme : gouv-fiscalite 

 Package principal : com.gouv.fiscalite
    
- .admin : dédié à la disposition de tout ce qui concerne l'administratif

        - .societe : les différents objets permettant de manipuler l'administratif : Siege Social, Categorie de societe, Siret ...
         
        - => Tests 
            - SiretTests : test de la fonction qui permet de verifier l'authenticité du numéro de Siret
    
- .gestion : dédié à la disposition de tout ce qui concerne la gestion

        - .societe : gestion du calcul de l'Impôt sur la societe en fonction du Chiffre d'affaire et du Taux
        

        - => Tests
            - ImpotSocieteTests : test de la fonction qui permet de calculer l'impôt sur la societe
            - TauxImpotTests : test de la fonction qui permet de selectionner le bon taux en fonction de la categorie de la societe
            
- .exceptions : dédié aux controles des différentes rêgles de données => Siret, Chiffre d'affaire

    
